﻿
cors = require('cors');
express = require('express');

var app = express();
app.use(cors());

app.listen(3001, function () {
	console.log('Server running at http://127.0.0.1:3001/');
});


app.get('/', function (req, res) {
	var a = req.query.a ? req.query.a : 0;
    var b = req.query.b ? req.query.b: 0;
    var result = a*1 + b*1;
	
	res.send(result.toString());
  
});